import random

def jogar():

    print("*********************************")
    print("Bem vindo ao jogo de Adivinhação!")
    print("*********************************\n")

    # random.seed() para defnir a semente da aleatoriedade
    numero_secreto = random.randrange(1, 101)
    total_de_tentativas = 0
    pontos = 1000

    print("Qual o nível de dificuldade? ")
    print("[ (1) Fácil  (2) Médio  (3) Difícil ]\n")
    nivel = int(input("Defina o nível: "))

    if (nivel == 1):
        total_de_tentativas = 20
    elif (nivel == 2):
        total_de_tentativas = 10
    else:
        total_de_tentativas = 5

    #while (rodada <= total_de_tentativas):
    for rodada in range(1, total_de_tentativas + 1):
        # ou print(f"Tentativa {rodada} de {total_de_tentativas}")
        print("Tentativa {} de {}".format(rodada, total_de_tentativas))
        chute = int(input("Digite o seu número entre 1 e 100: "))
        print("Você digitou", chute)

        if (chute < 1 or chute > 100):
            print("Você deve digitar um número entre 1 e 100!")
            continue

        acertou = chute == numero_secreto
        chute_maior = chute > numero_secreto
        chute_menor = chute < numero_secreto

        if (acertou):
            print(f"\nVocê acertou e fez {pontos} pontos!\n")
            break
        else:
            if (chute_maior):
                print("\nVocê errou! O seu chute foi MAIOR do que o Número Secreto.")
            elif (chute_menor):
                print("\nVocê errou! O seu chute foi MENOR do que o Número Secreto.")
            pontos_perdidos = abs(numero_secreto - chute)
            pontos = pontos - pontos_perdidos

    print("FIM DE JOGO\n")

if (__name__ == "__main__"):
    jogar()

