

class Conta:

    def __init__(self, numero, titular, saldo, limite = 1000.0):
        print("Construindo o objeto ... {}".format(self))
        self.__numero = numero
        self.__titular = titular
        self.__saldo = saldo
        self.__limite = limite

    def extrato(self):
        print(f" Saldo de R$ {self.__saldo} do titular {self.__titular}.")

    def depositar(self, valor):
        if (valor + self.__saldo) > self.__limite:
            print(" Você está a depositar um valor que faz com que seja atingido o limite da conta")
        else:
            self.__saldo += valor

    def __nao_pode_sacar(self, valor_a_sacar):
        valor_disponivel_a_sacar = self.__saldo + self.__limite
        return valor_a_sacar >= valor_disponivel_a_sacar

    def sacar(self, valor):
        if (self.__nao_pode_sacar(valor)):
            print(" Você está a sacar um valor maior do que o limite.")
        else:
            self.__saldo -= valor

    def transferir(self, valor, destino):
        if valor > self.__saldo or (valor + self.__saldo) > self.__limite:
            print(" Ultrapassou o Limite da conta depositada ou o Valor a ser depositado não existe.")
        else:
            self.sacar(valor)
            destino.depositar(valor)

    @property
    def saldo(self):
        return self.__saldo

    @property
    def limite(self):
        return self.__limite

    @property
    def numero(self):
        return self.__numero

    @property
    def titular(self):
        return self.__titular

    @limite.setter
    def limite(self, limite):
        self.__limite = limite

    @staticmethod
    def codigo_banco():
        return "001"

    @staticmethod
    def codigos_bancos():
        return {'BB': '001', 'Caixa': '104', 'Bradesco':'237'}