class Programa: #classe mãe
    def __init__(self, nome, ano): #construtora + atributos
        self._nome = nome.title() #title -> formatação de Título
        self.ano = ano
        self._likes = 0

    @property #getter
    def likes(self):
        return self._likes

    def dar_like(self): #método
        self._likes += 1

    @property #getter
    def nome(self):
        return self._nome

    @nome.setter #setter
    def nome(self, novo_nome):
        self._nome = novo_nome.title()

    def __str__(self):
        return f"{self._nome} -  {self.ano} - {self._likes} Likes"


class Filme(Programa): #vai herdar a classe mãe
    def __init__(self, nome, ano, duracao):
        super().__init__(nome, ano) # vai acessar o construtor
        # da classe mãe e chamar qualquer um de seus atributos ou métodos
        self.duracao = duracao

    def __str__(self):
        return f"{self._nome} -  {self.ano} - {self.duracao} min. - {self._likes} Likes"


class Serie(Programa): #vai herdar a classe mãe
    def __init__(self, nome, ano, temporadas):
        super().__init__(nome, ano)
        self.temporadas = temporadas

    def __str__(self):
        return f"{self._nome} -  {self.ano} - {self.temporadas} temp. - {self._likes} Likes"

#usando a herança, diminuímos a repetição do código

class Playlist:
    def __init__(self, nome, programas):
        self.nome = nome
        self._programas = programas

    def __getitem__(self, item):
        return self._programas[item]

    @property
    def listagem(self):
        return self._programas

    def __len__(self):
        return len(self._programas)



vingadores_gi = Filme("Vingadores|Guerra Infinita", 2018, 160) #objeto da classe Filme
atlanta = Serie("Atlanta", 2018, 2) #objeto da classe Serie
tmep = Filme("Todo mundo em pânico", 1999, 100)
demolidor = Serie("Demolidor", 2016, 2)

tmep.dar_like()
tmep.dar_like()
tmep.dar_like()
vingadores_gi.dar_like()
vingadores_gi.dar_like()
vingadores_gi.dar_like()
vingadores_gi.dar_like()
demolidor.dar_like()
demolidor.dar_like()
demolidor.dar_like()
atlanta.dar_like()
atlanta.dar_like()

filmes_e_series = [vingadores_gi, atlanta, demolidor, tmep]
playlist_fds = Playlist("Fim de Semana", filmes_e_series)

print(f"Tamanho da Playlist: {len(playlist_fds)}")

#polimorfismo
for programa in playlist_fds:
    print(programa)



