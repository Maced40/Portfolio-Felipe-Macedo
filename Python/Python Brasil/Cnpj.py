exemplo_cnpj = "35379838000112"
# cnpj = CNPJ()
# print(cnpj.validate(exemplo_cnpj))

from validate_docbr import CPF, CNPJ

class CpfCnpj:
  def __init__(self, documento, tipo_documento):
    self.tipo_documento = tipo_documento
    documento = str(documento)
    if self.tipo_documento == "cpf" or self.tipo_documento == "CPF":
      if self.cpf_e_valido(documento):
        self.cpf = documento
      else:
        raise ValueError("CPF inválido!")
    elif self.tipo_documento == "cnpj" or self.tipo_documento == "CNPJ":
      if self.cnpj_e_valido(documento):
       self.cnpj = documento
      else:
         raise ValueError("CNPJ inválido!")
    else:
      raise ValueError("Documento inválido")

  # validando cpf e cnpj com validate
  def cpf_e_valido(self, cpf):
    if len(cpf) == 11:
      validador = CPF() 
      return validador.validate(cpf)
    else:
      raise ValueError("Quantidade de dígitos inválida!!")

  def cnpj_e_valido(self, cnpj):
    if len(cnpj) == 14:
      validate_cnpj = CNPJ()
      return validate_cnpj.validate(cnpj)
    else:
      raise ValueError("Quantidade de dígitos inválida!!")

  # formatando o cpf e o cnpj com mask
  def format_cpf(self):
    mascara = CPF()
    return mascara.mask(self.cpf)

  def format_cnpj(self):
    mascara = CNPJ()
    return mascara.mask(self.cnpj)

  def __str__(self):
    if self.tipo_documento == "cpf" or self.tipo_documento == "CPF":
      return self.format_cpf()
    elif self.tipo_documento == "cnpj" or self.tipo_documento == "CNPJ":
      return self.format_cnpj()

documento = CpfCnpj(exemplo_cnpj, 'cnpj')
print(documento)

exemplo_cpf = "29030616024"
documento = CpfCnpj(exemplo_cpf, 'cpf')
print(documento)

# refatoração


class Documento:
  @staticmethod
  def cria_documento(documento):
    if len(documento) == 11:
      return DocCpf(documento)
    elif len(documento) == 14:
      return DocCnpj(documento)
    else:
      raise ValueError("Quantidade de dígitos está incorreta!!")

class DocCpf:
  def __init__(self, documento):
    if self.valida(documento):
      self.cpf = documento
    else:
      raise ValueError("CPf inválido!")

  def __str__(self):
    return self.format()

  def valida(self, documento):
    validador = CPF()
    return validador.validate(documento)

  def format(self):
    mascara = CPF()
    return mascara.mask(self.cpf)

class DocCnpj:
  def __init__(self, documento):
    if self.valida(documento):
      self.cnpj = documento
    else:
      raise ValueError("Cnpj inválido!")

  def __str__(self):
    return self.format()

  def valida(self, documento):
    validador = CNPJ()
    return validador.validate(documento)

  def format(self):
    mascara = CNPJ()
    return mascara.mask(self.cnpj)

exemplo_cnpj = "35379838000112"
documento = Documento.cria_documento(exemplo_cnpj)
print(documento)

exemplo_cpf = "29030616024"
documento = Documento.cria_documento(exemplo_cpf)
print(documento)