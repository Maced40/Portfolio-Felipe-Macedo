import re

padrao = "[0-9][a-z]{2}[0-9]"
texto = "123 1ac2 1cc aal"
resposta = re.search(padrao, texto)
print(resposta.group())

padrao = "\w{5,50}@\w{3,10}.\w{2,3}.\w{2,3}"
texto = 'aaabbbcc rodrigo123@gmail.com.br y4378rg34ry34y834vy3u4t745t'
resposta = re.search(padrao, texto)

print(resposta.group())

padrao = "\w{5,50}@[a-z]{3,10}.com.br"
texto = "aaabbbcc rodrigo123@gmail.com.br ccbbbaaa"
resposta = re.search(padrao, texto)

print(resposta.group())

padrao_molde = "(xx) aaaa-wwww"
padrao = "[0-9]{2}[0-9]{4,5}[0-9]{4}"
texto = "eu gosto do numero 2126451234 e gosto também do 2136431234"
resposta = re.findall(padrao, texto)

print(resposta)

# agora sim, a classe...


class TelefoneBr:
  def __init__(self, telefone):
    if self.valida_telefone(telefone):
      self.numero = telefone
    else:
      raise ValueError("Telefone inválido!")

  def __str__(self):
    return self.format_numero()

  def valida_telefone(self, telefone):
    padrao = "([0-9]{2,3})?([0-9]{2})([0-9]{4,5})([0-9]{4})"
    resposta = re.findall(padrao, telefone)
    if resposta:
      return True
    else:
      return False

  def format_numero(self):
    padrao = "([0-9]{2,3})?([0-9]{2})([0-9]{4,5})([0-9]{4})"
    resposta = re.search(padrao, self.numero)
    numero_formatado = f"+{resposta.group(1)} ({resposta.group(2)}) {resposta.group(3)}-{resposta.group(4)}"
    return numero_formatado




telefone = "552126483344"

telefone_objeto = TelefoneBr(telefone)
print(telefone_objeto)