'''
class Cpf:
  def __init__(self, documento):
    documento = str(documento)
    if self.cpf_eh_Valido(documento):
      self.cpf = documento
    else:
      raise ValueError("CPF inválido!")

  def __str__(self):
    return self.format_cpf()

  def cpf_eh_Valido(self, documento):
    if len(documento) == 11:
      return True
    else:
      return False
  # formatando o cpf
  def format_cpf(self):
    fatia_um = self.cpf[:3]
    fatia_dois = self.cpf[3:6]
    fatia_tres = self.cpf[6:9]
    fatia_quatro = self.cpf[9:]
    return f"{fatia_um}.{fatia_dois}.{fatia_tres}-{fatia_quatro}"
'''
from validate_docbr import CPF

class Cpf:
  def __init__(self, documento):
    documento = str(documento)
    if self.cpf_e_valido(documento):
      self.cpf = documento
    else:
      raise ValueError("CPF inválido!")

  # validando um cpf com validate
  def cpf_e_valido(self, cpf):
    if len(cpf) == 11:
      validador = CPF() 
      return validador.validate(cpf)
    else:
      raise ValueError("Quantidade de dígitos inválida!!")

  # formatando o cpf com mask
  def format_cpf(self):
    mascara = CPF()
    return mascara.mask(self.cpf)

  def __str__(self):
    return self.format_cpf()

cpf = "15616934143"

objeto_cpf = Cpf(cpf)
print(objeto_cpf)

cpf_um = Cpf("15316264754")
print(cpf_um)